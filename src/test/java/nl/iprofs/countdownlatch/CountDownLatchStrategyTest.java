package nl.iprofs.countdownlatch;

import org.junit.Before;
import org.junit.Test;

public class CountDownLatchStrategyTest {

    private SomeStrategy cdlImprovedStrat;
    private SomeStrategy cdlDefaultStrat;

    @Before
    public void setup() {
        cdlDefaultStrat = new CountDownLatchDefaultStrategy();
        cdlImprovedStrat = new CountDownLatchImprovedStrategy();
        
    }

    @Test
    public void testDefaultStrategy() throws Exception {
        cdlDefaultStrat.setNumberOfWorkers(50);
        cdlDefaultStrat.execute();
    }

    @Test
    public void testImprovedStrategy() throws Exception {
        cdlImprovedStrat.setNumberOfWorkers(50);
        cdlImprovedStrat.execute();
    }
}
