package nl.iprofs.countdownlatch;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CountDownLatchImprovedStrategy implements SomeStrategy {
    
    private final static Logger logger = LoggerFactory.getLogger(CountDownLatchImprovedStrategy.class);
    
    private int numberOfWorkers;
    public void execute() throws InterruptedException {


        CountDownLatch startSignal = new CountDownLatch(numberOfWorkers+1);
        CountDownLatch doneSignal = new CountDownLatch(numberOfWorkers);

        final long startTime = System.currentTimeMillis();
        
        for (int i = 0; i < numberOfWorkers; ++i)
            // create and start threads
            new Thread(new Worker(startSignal, doneSignal, startTime)).start();

        doSomethingElse(); // don't let run yet
        startSignal.countDown(); // let all threads proceed
        doSomethingElse();
        doneSignal.await(); // wait for all to finish
    }

    private void doSomethingElse() {
    }

    public void setNumberOfWorkers(int numberOfWorkers) {
        this.numberOfWorkers = numberOfWorkers;
    }

    public static class Worker implements Runnable {
        private final CountDownLatch startSignal;
        private final CountDownLatch doneSignal;
        private final long startTime;

        Worker(CountDownLatch startSignal, CountDownLatch doneSignal, long startTime) {
            this.startSignal = startSignal;
            this.doneSignal = doneSignal;
            this.startTime = startTime;
        }

        public void run() {
            try {
                initializeWorkerPotentiallySlow();
                startSignal.countDown();
                startSignal.await();
                doWork();
                doneSignal.countDown();
            } catch (InterruptedException ex) {
            }
        }

        /**
         * Initialization that takes up to 100 ms to complete. 
         */
        private void initializeWorkerPotentiallySlow() throws InterruptedException {
            Thread.sleep(new Random().nextInt(100));
        }

        void doWork() {
            long timeOfReleaseDelta = System.currentTimeMillis() - startTime; 
            logger.debug(String.valueOf(timeOfReleaseDelta));
            
        }
    }

}
