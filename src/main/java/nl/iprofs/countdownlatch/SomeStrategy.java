package nl.iprofs.countdownlatch;

public interface SomeStrategy {

    void execute() throws InterruptedException;
    
    void setNumberOfWorkers(int nr);
}
